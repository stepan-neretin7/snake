from lib import GameAnnouncement, GameConfig, GamePlayers
from network.topology import NetworkTopology


class GameCreator:
    def __init__(self, network_manager: NetworkTopology):
        self.game_config = GameConfig(30, 30, 2, 2)
        self.players = GamePlayers()
        self.game_name = "test game"
        self.network_manager = network_manager

    def get_announcement_msg(self) -> GameAnnouncement:
        return GameAnnouncement(game_name=self.game_name, players=self.players, config=self.game_config)
