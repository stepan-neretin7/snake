
import threading

from game.game_creator import GameCreator
from network.send_socket import SendSocket


class AnnouncementThread(threading.Thread):
    def __init__(self, sock: SendSocket, game_info: GameCreator):
        super().__init__()
        self.socket = sock
        self.stop_event = threading.Event()
        self.game_info = game_info

    def send_message(self):
        self.socket.send(bytes(self.game_info.get_announcement_msg()))

    def run(self):
        while not self.stop_event.is_set():
            self.send_message()
            self.stop_event.wait(1)

    def start(self):
        super().start()
