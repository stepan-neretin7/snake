from config import Config
from game.game_creator import GameCreator
from game.threads.announcement import AnnouncementThread
from network.real_impl.multicast_receive import MulticastReceiveSocket
from network.real_impl.udp_send_socket import UDPSendSocket
from network.topology import NetworkTopology


class GameManager:
    def __init__(self, config: Config):
        self.config: Config = config
        multicast_addr: tuple = (self.config.get_multicast_group, self.config.get_port)
        self.network_manager: NetworkTopology = NetworkTopology(
            MulticastReceiveSocket(*multicast_addr),
            UDPSendSocket(multicast_addr)
        )

    def start_game(self):
        self.network_manager.start_listen()
        game_creator = GameCreator(self.network_manager)
        # AnnouncementThread(self.network_manager.get_send_sock(), game_creator).start()


if __name__ == "__main__":
    try:
        t = GameManager(Config())
        t.start_game()
    except KeyboardInterrupt:
        print("asdasd")
