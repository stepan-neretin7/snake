import socket

from network.send_socket import SendSocket


class UDPSendSocket(SendSocket):
    def __init__(self, address=None):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
        self.address = address

    def connect(self, address):
        self.sock.connect(address)

    def send(self, data, address=None):
        if address is not None:
            self.sock.sendto(data, address)
            return

        self.sock.sendto(data, self.address)
