import socket
import struct

from network.receive_socket import ReceiveSocket


class MulticastReceiveSocket(ReceiveSocket):
    def __init__(self, multicast_group: str, multicast_port: int):
        super().__init__(multicast_group, multicast_port)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setblocking(False)
        self.bind()

    def bind(self):
        self.sock.bind((self.multicast_group, self.multicast_port))
        mreq = struct.pack("4sl", socket.inet_aton(self.multicast_group), socket.INADDR_ANY)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

    def recv(self, buf_size: int):
        return self.sock.recv(buf_size)
