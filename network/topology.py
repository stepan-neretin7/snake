import threading

from lib import GameAnnouncement
from network.real_impl.multicast_receive import MulticastReceiveSocket
from network.real_impl.udp_send_socket import UDPSendSocket
from network.receive_socket import ReceiveSocket
from network.send_socket import SendSocket


class NetworkTopology:
    def __init__(self, receive_sock: ReceiveSocket, send_sock: SendSocket):
        self.receive_thread: threading.Thread = threading.Thread(target=self.receive_data)
        self.receive_sock: ReceiveSocket = receive_sock
        self.send_sock: SendSocket = send_sock

    def receive_data(self):
        while True:
            data = self.receive_sock.recv(1024)
            print(f"Received message: {GameAnnouncement().parse(data)}")

    def start_listen(self):
        self.receive_thread.start()

    def get_send_sock(self) -> SendSocket:
        return self.send_sock


if __name__ == "__main__":
    conf = ("239.255.255.254", 446)
    t = NetworkTopology(MulticastReceiveSocket(*conf), UDPSendSocket(*conf))
    print(t)
