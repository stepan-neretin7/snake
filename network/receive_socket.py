import abc


class ReceiveSocket(metaclass=abc.ABCMeta):
    def __init__(self, multicast_group: str, multicast_port: int):
        self.multicast_group = multicast_group
        self.multicast_port = multicast_port

    @abc.abstractmethod
    def bind(self):
        pass

    @abc.abstractmethod
    def recv(self, buf_size: int):
        pass
