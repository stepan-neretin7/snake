import abc


class SendSocket(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def connect(self, address):
        pass

    @abc.abstractmethod
    def send(self, data, address=None):
        pass
