class ReceiveSocket:
    def __init__(self, multicast_group: str, multicast_port: int):
        self.multicast_group = multicast_group
        self.multicast_port = multicast_port
        self.recv_buffer = ""

    def bind(self):
        pass

    def recv(self, buf_size: int):
        if len(self.recv_buffer) <= buf_size:
            data = self.recv_buffer
            self.recv_buffer = ""
            return data.encode(), ("0", 0)
        else:
            data = self.recv_buffer[:buf_size]
            self.recv_buffer = self.recv_buffer[buf_size:]
            return data.encode(), ("", 0)

    def set_recv_buffer(self, data):
        self.recv_buffer = data


class SendSocket:
    def __init__(self):
        self.send_buffer = ""

    def connect(self, address):
        pass

    def send(self, data):
        self.send_buffer += data

    def get_send_buffer(self):
        return self.send_buffer


class StringSocket(ReceiveSocket, SendSocket):
    def __init__(self, multicast_group: str, multicast_port: int):
        ReceiveSocket.__init__(self, multicast_group, multicast_port)
        SendSocket.__init__(self)
