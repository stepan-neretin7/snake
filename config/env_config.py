import os

from dotenv import load_dotenv

load_dotenv()


class Config:
    @property
    def get_multicast_group(self) -> str:
        return os.getenv("MULTICAST_GROUP", "239.255.255.254")

    @property
    def get_port(self) -> int:
        return int(os.getenv("PORT", 5000))
